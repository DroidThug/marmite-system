# Stock System Framework of Wileyfox Swift 2

## Spec Sheet

| Feature                 | Specification                     |
| :---------------------- | :-------------------------------- |
| CPU                     | Octa-core 1.4 GHz Cortex-A53      |
| Chipset                 | Qualcomm MSM8937 Snapdragon 430   |
| GPU                     | Adreno 505                        |
| Memory                  | 2 GB                              |
| Shipped Android Version | 7.1.1                             |
| Storage                 | 16 GB                             |
| MicroSD                 | Up to 64 GB                       |
| Battery                 | 2700 mAH (non-removable)          |
| Dimensions              | 143.7 x 71.9 x 8.6 mm             |
| Display                 | 720 x 1280 pixels, 5.0" IPS LCD   |
| Rear Camera             | 13 MP, Dual-LED flash             |
| Front Camera            | 8 MP                              |
| Release Date            | October 2016                      |

## Device Picture

![Wileyfox Swift 2](https://www.wileyfox.com/skin/frontend/aztec/wileyfox/spark/img/hero-midnight-front.png "Wileyfox Swift 2")

## Copyright

```
#
# Copyright (C) 2017 Wileyfox
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
