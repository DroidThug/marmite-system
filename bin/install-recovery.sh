#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:20131096:45f479268c96e81ff537e5a29cdcbd96c06b8e65; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:17659156:3165c9c759af1e6e367077f3111a211d75963fc5 EMMC:/dev/block/bootdevice/by-name/recovery 45f479268c96e81ff537e5a29cdcbd96c06b8e65 20131096 3165c9c759af1e6e367077f3111a211d75963fc5:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
